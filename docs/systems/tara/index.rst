.. _tara-system:

======================
Tara Cluster
======================

.. toctree::
    :maxdepth: 2

    timeline.rst
    overview.rst
    installation.rst
    slurm.rst
    appendices.rst
    
   