==================
System Overview
==================


General Information
=====================

| **slurm Version**: 18.08.5 (Jan 30, 2019)
| **munge Version**: 0.5.13 (Sep 27, 2017)
| **PMIx Version** : 3.0.2 (Sep 19, 2018)
| **nhc Version**  : 1.4.2 (Nov 12, 2015)
| **UCX Version**  : 1.4.0 (Oct 30, 2018)
| **lmod Version** : 6.6.3
| **EasyBuild Version** : 3.8


| **slurm user**: ``slurm``
| **slurm UID**: ``2001``
| **slurm group**: ``slurm``
| **slurm GID**: ``2001``

| **munge user**: ``munge``
| **munge UID**: ``2000``
| **munge group**: ``munge``
| **munge GID**: ``2000``

| **module user**: ``modules``
| **module UID**: ``2002``
| **module group**: ``modules``
| **module GID**: ``2002``

.. _prerequisites:

Initial Setup
===============

* *Clean* installation of CentOS 7. (If possible, the initial state of Tara cluster machines.)
* An NFS storage mounted to all machines. (Can we create a virtual HDD and mount it to all machine?)

Machine Configuration
======================

============  ====================  ================  ===============  ===============  ======
Node Class    NodeName              IP (InfiniBand)   IP (1GbE)        IP (Ext)         Notes
============  ====================  ================  ===============  ===============  ======
frontend      tara-frontend-1       172.20.1.2        172.21.1.2       10.226.33.1      
============  ====================  ================  ===============  ===============  ======

Machine Layout
----------------

.. Adding users
.. ===============

.. * Add user in ``freeipa``
.. * Add user in SLURM through ``sacctmgr``. command ? 
