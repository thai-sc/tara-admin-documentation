===========
Appendices
===========


List of Installed Packages
============================

MUNGE installation

.. code:: 

    $ yum install rpm-build wget

    Installing:
     rpm-build                  x86_64     4.11.3-32.el7              base        147 k
     wget                       x86_64     1.14-15.el7_4.1            base        547 k
    Installing for dependencies:
     bzip2                      x86_64     1.0.6-13.el7               base         52 k
     dwz                        x86_64     0.11-3.el7                 base         99 k
     elfutils                   x86_64     0.170-4.el7                base        282 k
     gdb                        x86_64     7.6.1-110.el7              base        2.4 M
     patch                      x86_64     2.7.1-10.el7_5             updates     110 k
     perl                       x86_64     4:5.16.3-292.el7           base        8.0 M
     perl-Carp                  noarch     1.26-244.el7               base         19 k
     perl-Encode                x86_64     2.51-7.el7                 base        1.5 M
     perl-Exporter              noarch     5.68-3.el7                 base         28 k
     perl-File-Path             noarch     2.09-2.el7                 base         26 k
     perl-File-Temp             noarch     0.23.01-3.el7              base         56 k
     perl-Filter                x86_64     1.49-3.el7                 base         76 k
     perl-Getopt-Long           noarch     2.40-3.el7                 base         56 k
     perl-HTTP-Tiny             noarch     0.033-3.el7                base         38 k
     perl-PathTools             x86_64     3.40-5.el7                 base         82 k
     perl-Pod-Escapes           noarch     1:1.04-292.el7             base         51 k
     perl-Pod-Perldoc           noarch     3.20-4.el7                 base         87 k
     perl-Pod-Simple            noarch     1:3.28-4.el7               base        216 k
     perl-Pod-Usage             noarch     1.63-3.el7                 base         27 k
     perl-Scalar-List-Utils     x86_64     1.27-248.el7               base         36 k
     perl-Socket                x86_64     2.010-4.el7                base         49 k
     perl-Storable              x86_64     2.45-3.el7                 base         77 k
     perl-Text-ParseWords       noarch     3.29-4.el7                 base         14 k
     perl-Thread-Queue          noarch     3.02-2.el7                 base         17 k
     perl-Time-HiRes            x86_64     4:1.9725-3.el7             base         45 k
     perl-Time-Local            noarch     1.2300-2.el7               base         24 k
     perl-constant              noarch     1.27-2.el7                 base         19 k
     perl-libs                  x86_64     4:5.16.3-292.el7           base        688 k
     perl-macros                x86_64     4:5.16.3-292.el7           base         43 k
     perl-parent                noarch     1:0.225-244.el7            base         12 k
     perl-podlators             noarch     2.5.1-3.el7                base        112 k
     perl-srpm-macros           noarch     1-8.el7                    base        4.6 k
     perl-threads               x86_64     1.87-4.el7                 base         49 k
     perl-threads-shared        x86_64     1.43-6.el7                 base         39 k
     redhat-rpm-config          noarch     9.1.0-80.el7.centos        base         79 k
     unzip                      x86_64     6.0-19.el7                 base        170 k
     zip                        x86_64     3.0-11.el7                 base        260 k

.. code::
    
    $ yum install gcc bzip2-devel openssl-devel zlib-devel

    Installing:
     gcc                       x86_64       4.8.5-28.el7_5.1        updates        16 M
     gcc-c++                   x86_64       4.8.5-28.el7_5.1        updates       7.2 M
     bzip2-devel               x86_64       1.0.6-13.el7            base          218 k
     openssl-devel             x86_64       1:1.0.2k-12.el7         base          1.5 M
     zlib-devel                x86_64       1.2.7-17.el7            base           50 k
    Installing for dependencies:
     cpp                       x86_64       4.8.5-28.el7_5.1        updates       5.9 M
     glibc-devel               x86_64       2.17-222.el7            base          1.1 M
     glibc-headers             x86_64       2.17-222.el7            base          678 k
     kernel-headers            x86_64       3.10.0-862.14.4.el7     updates       7.1 M
     libmpc                    x86_64       1.0.1-3.el7             base           51 k
     libstdc++-devel           x86_64       4.8.5-28.el7_5.1        updates       1.5 M
     mpfr                      x86_64       3.1.1-4.el7             base          203 k
     keyutils-libs-devel       x86_64       1.5.8-3.el7             base           37 k
     krb5-devel                x86_64       1.15.1-19.el7           updates       269 k
     libcom_err-devel          x86_64       1.42.9-12.el7_5         updates        31 k
     libselinux-devel          x86_64       2.5-12.el7              base          186 k
     libsepol-devel            x86_64       2.5-8.1.el7             base           77 k
     libverto-devel            x86_64       0.2.5-4.el7             base           12 k
     pcre-devel                x86_64       8.32-17.el7             base          480 k

Slurm Installation 

.. code::
    
    $ yum install libtool libevent-devel

    Installing:
     libtool                   x86_64         2.4.2-22.el7_3         base         588 k
     libevent-devel            x86_64         2.0.21-4.el7           base           85 k
    Installing for dependencies:
     autoconf                  noarch         2.69-11.el7            base         701 k
     automake                  noarch         1.13.4-3.el7           base         679 k
     m4                        x86_64         1.4.16-10.el7          base         256 k
     perl-Data-Dumper          x86_64         2.145-3.el7            base          47 k
     perl-Test-Harness         noarch         3.28-3.el7             base         302 k

.. code::

    $ yum install readline-devel perl-ExtUtils-MakeMaker pam-devel hwloc-devel freeipmi-devel lua-devel mysql-devel libssh2-devel

    Installing:
     perl-ExtUtils-MakeMakernoarch        6.68-3.el7               base        275 k
     readline-devel         x86_64        6.2-10.el7               base        138 k
     hwloc-devel            x86_64        1.11.8-4.el7             base        208 k
     lua-devel              x86_64        5.1.4-15.el7             base         21 k
     mariadb-devel          x86_64        1:5.5.60-1.el7_5         updates     754 k
     pam-devel              x86_64        1.1.8-22.el7             base        184 k
     libssh2-devel          x86_64        1.4.3-10.el7_2.1         base         54 k
     freeipmi-devel         x86_64        1.5.7-2.el7              base        260 k
    Installing for dependencies:
     gdbm-devel             x86_64        1.10-8.el7               base         47 k
     libdb-devel            x86_64        5.3.21-24.el7            base         38 k
     ncurses-devel          x86_64        5.9-14.20130511.el7_4    base        712 k
     perl-ExtUtils-Install  noarch        1.58-292.el7             base         74 k
     perl-ExtUtils-Manifest noarch        1.61-244.el7             base         31 k
     perl-ExtUtils-ParseXS  noarch        1:3.18-3.el7             base         77 k
     perl-devel             x86_64        4:5.16.3-292.el7         base        453 k
     pyparsing              noarch        1.5.6-9.el7              base         94 k
     systemtap-sdt-devel    x86_64        3.2-8.el7_5              updates      73 k
     hwloc-libs             x86_64        1.11.8-4.el7             base        1.6 M
     ibacm                  x86_64        15-7.el7_5               updates      77 k
     libibcm                x86_64        15-7.el7_5               updates      15 k
     libibumad              x86_64        15-7.el7_5               updates      22 k
     libibverbs             x86_64        15-7.el7_5               updates     224 k
     librdmacm              x86_64        15-7.el7_5               updates      61 k
     libtool-ltdl           x86_64        2.4.2-22.el7_3           base         49 k
     rdma-core-devel        x86_64        15-7.el7_5               updates     203 k
     OpenIPMI-modalias      x86_64        2.0.23-2.el7             base         16 k
     freeipmi               x86_64        1.5.7-2.el7              base        2.0 M

Lmod Installation

.. code::

    $ yum install lmod

    Installing:
     Lmod                    x86_64          6.6.3-1.el7               epel          192 k
    Installing for dependencies:
     lua-bitop               x86_64          1.0.2-3.el7               epel          7.9 k
     lua-filesystem          x86_64          1.6.2-2.el7               epel           28 k
     lua-json                noarch          1.3.2-2.el7               epel           23 k
     lua-lpeg                x86_64          0.12-1.el7                epel           59 k
     lua-posix               x86_64          32-2.el7                  epel          116 k
     lua-term                x86_64          0.03-3.el7                epel           10 k
     tcl                     x86_64          1:8.5.13-8.el7            base          1.9 M

Git Installation

.. code::

    $ yum install git

    Installing:
     git                     x86_64        1.8.3.1-14.el7_5        updates        4.4 M
    Installing for dependencies:
     libgnome-keyring        x86_64        3.12.0-1.el7            base           109 k
     perl-Error              noarch        1:0.17020-2.el7         base            32 k
     perl-Git                noarch        1.8.3.1-14.el7_5        updates         54 k
     perl-TermReadKey        x86_64        2.30-20.el7             base            31 k
     rsync                   x86_64        3.1.2-4.el7             base           403 k
