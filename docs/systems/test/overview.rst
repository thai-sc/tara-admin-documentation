==================
System Overview
==================


General Information
=====================

| **slurm Version**: 18.08.1 (Oct 8, 2018)
| **munge Version**: 0.5.13 (Sep 27, 2017)
| **PMIx Version** : 3.0.2 (Sep 19, 2018)
| **nhc Version**  : 1.4.2 (Nov 12, 2015)
| **lmod Version** : 6.6.3
| **EasyBuild Version** : 3.7.1


| **slurm user**: ``slurm``
| **slurm user environment variable**: ``SLURMUSER``
| **slurm UID**: ``2001``
| **slurm group**: ``slurm``
| **slurm GID**: ``2001``

| **munge user**: ``munge``
| **munge user environment variable**: ``MUNGEUSER``
| **munge UID**: ``2000``
| **munge group**: ``munge``
| **munge GID**: ``2000``

| **module user**: ``modules``
| **module UID**: ``2003``
| **module group**: ``modules``
| **module GID**: ``2002``

.. _prerequisites:

Initial Setup
===============

* *Clean* installation of CentOS 7. (If possible, the initial state of Tara cluster machines.)
* An NFS storage mounted to all machines. (Can we create a virtual HDD and mount it to all machine?)

Machine Configuration
======================

============  ====================  ================  ===============  ===============  ======
Node Class    NodeName              IP (InfiniBand)   IP (1GbE)        IP (Ext)         Notes
============  ====================  ================  ===============  ===============  ======
frontend      tara-frontend-1       172.20.1.2        172.21.1.2       10.226.33.1      

freeipa       freeipa                                 172.21.5.2       10.226.33.2      VM
slurmctld     slurmctld                               172.21.5.3       10.226.33.3      VM
slurmdbd      slurmdbd                                172.21.5.4       10.226.33.4      VM
mysql         mysql                                   172.21.5.5       10.226.33.5      VM
dns           dns                                     172.21.5.6       10.226.33.6      VM

compute       tara-c-[001-006]      172.20.10.1 -     172.21.10.1 -    10.226.33.7-      
                                    172.20.10.6       172.21.10.6      10.226.33.12
memory        tara-m-[001-002]      172.20.20.1 -     172.21.20.1 -    10.226.33.13-    FAT nodes
                                    172.20.20.2       172.21.20.2      10.226.33.14
dgx           tara-dgx1-[001-002]   172.20.31.1 -     172.21.31.1 -    10.226.33.15-    dgx1 is reserved. 
                                    172.20.31.2       172.21.31.2      10.226.33.16

nfs           nfs                                     172.20.2.1       10.226.33.18      VM 
============  ====================  ================  ===============  ===============  ======

Machine Layout
----------------

.. image:: testing-diagram.jpg

.. Adding users
.. ===============

.. * Add user in ``freeipa``
.. * Add user in SLURM through ``sacctmgr``. command ? 
