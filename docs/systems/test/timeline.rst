================
Timeline
================

Wednesday meeting: Set weekly goals and progress update? 

.. tabularcolumns:: |l|p{14cm}|

================  ============================== 
Important Date    Plan            
================  ============================== 
16 Oct 2018       Initial meeting. 
24 Oct 2018       **Implementation**: Prepare the machines to the :ref:`prerequisites` state and **document** for initializing the machines. 
                     
                  **Testing**: Prepare the plan and, if possible, automate script to test initail services in :ref:`prerequisites`.
31 Oct 2018       **Implementation**: Implementation document for SLURM
                     
                  **Testing**: Plan and testing script for SLURM. 
16 Nov 2018       **Implementation**: 

                  * New VM environment 
                  * SLURM plugins: 

                    * PAM 
                    * cgroup 
                    * nhc  
                    * PMIx

                  * SLURM preemption
                  * Setting up separate DNS using BIND
                  * Setting up lmod/EasyBuild
                
                    * ``foss``
                    * ``mpi-foss``
                    * ``lampps``

                  * freeipa-server + free-client. 
                    
                  **Config**:

                  * ``gres.conf`` -- *REMOVED* Both GPU and DGX-1 will be exclusively allocated, so there is no need for GRES scheduling. 
                  * ``topology.conf``
                  * Code repository for SLURM config.

                  **Testing**: 

                  * SLURM basic function
                  * VM Topology
                  * freeipa + dns separation 
                  * Installation document

23 Nov 2018       **Implementation**: 

                  * Implement 2nd VM based on existing document
                     
                  **Testing**:                      

                  * Detailed test cases.

03 Dec 2018       **Implementation**: 

                  * Preemption 
                  * Slurm account management: users must be associated with account to be able to submit jobs.
                  * nhc

                  * MPI
                  * GCC
                  * VASP 5.4.4 (local)
                     
                  **Testing**:              

                  * More test cases.  
       
================  ============================== 

TBD
=====

* Should we allow a user to see others jobs in the queue? 
